﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InstantCredit.Helpers
{
    public class FileStorage
    {
        public string PassportFilesDirectory { get; set; }
        public string IncomStatementFilesDirectory { get; set; }
        public string FileServer { get; set; }
    }
}
