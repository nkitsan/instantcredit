﻿using InstantCredit.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Threading.Tasks;

namespace InstantCredit.Core
{
    public class FileService
    {
        private readonly FileStorage fileStorage;

        public FileService(IOptions<FileStorage> fileStorage)
        {
            this.fileStorage = fileStorage.Value;
        }

        public async Task<string> SavePassportFileAsync(IFormFile PassportFile)
        {
            return await SaveFileAsync(fileStorage.PassportFilesDirectory, PassportFile);
        }

        public async Task<string> SaveIncomeStatmentAsync(IFormFile IncomeStatmentFile)
        {
            return await SaveFileAsync(fileStorage.IncomStatementFilesDirectory, IncomeStatmentFile);
        }

        private async Task<string> SaveFileAsync(string filePath, IFormFile formFile)
        {
            var servername = Guid.NewGuid().ToString() + Path.GetExtension(formFile.FileName);
            var path = Path.Combine(Directory.GetCurrentDirectory(), filePath, servername);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await formFile.CopyToAsync(stream);
            }

            return servername;
        }
    }
}
