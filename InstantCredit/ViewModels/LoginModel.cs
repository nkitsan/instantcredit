﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InstantCredit.ViewModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Email not defined")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password not defined")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
