﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using InstantCredit.Models;
using System.ComponentModel.DataAnnotations;

namespace InstantCredit.ViewModels
{
    public class CreateApplicationModel
    {
        [Required(ErrorMessage = "Credit Plan not defined")]
        public int CreditPlan { get; set; }

        [Required(ErrorMessage = "Passport File not defined")]
        public IFormFile PassportFile { get; set; }

        [Required(ErrorMessage = "Income Statement File not defined")]
        public IFormFile IncomeStatementFile { get; set; }

        [Required(ErrorMessage = "Contact Phone not defined")]
        public string ContactPhone { get; set; }

        public List<CreditPlan> CreditPlans { get; set;}
    }
}
