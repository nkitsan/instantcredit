﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InstantCredit.ViewModels
{
    public class CreateCreditPlanModel
    {
        [Required(ErrorMessage = "Name not defined")]
        public string Name { get; set; }
       
        [Required(ErrorMessage = "Period not defined")]
        public int Period { get; set; }

        [Required(ErrorMessage = "Percents not defined")]
        public float Percents { get; set; }

        [Required(ErrorMessage = "Description not defined")]
        public string Description { get; set; }
    }
}
