﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InstantCredit.Models;

namespace InstantCredit.ViewModels
{
    public class CreditApplicationModel
    {
        public bool IsAdmin { get; set; }
        public List<CreditApplication> CreditApplications { get; set; }
    }
}
