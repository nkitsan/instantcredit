﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InstantCredit.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InstantCredit.Controllers
{
    public class PaymentController : Controller
    {

        private readonly InstantCreditContext db;

        public PaymentController(InstantCreditContext context)
        {
            db = context;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> IndexAsync()
        {
            User user = await db.Users.FirstOrDefaultAsync(user => user.Email == User.Identity.Name);

            return View();
        }
    }
}