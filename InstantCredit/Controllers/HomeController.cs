﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InstantCredit.Models;
using InstantCredit.Models.Enums;
using InstantCredit.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InstantCredit.Controllers
{
    public class HomeController : Controller
    {
        private readonly InstantCreditContext db;

        public HomeController(InstantCreditContext context)
        {
            db = context;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            var creditPlans = db.CreditPlans
                .Where(plan => plan.Status == CreditPlanStatus.Active)
                .ToList();

            return View(new CreditPlansModel() { IsAdmin = user.isAdmin, CreditPlans = creditPlans });
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (!user.isAdmin)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create(CreateCreditPlanModel model)
        {
            var user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (!user.isAdmin)
            {
                return RedirectToAction("Index", "Home");
            }

            if (ModelState.IsValid)
            {
                db.CreditPlans.Add(new CreditPlan 
                {
                    Name = model.Name,
                    Period = model.Period,
                    Percents = model.Percents,
                    Description = model.Description
                });
                await db.SaveChangesAsync();
                
                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Deactivate(int id)
        {
            var user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (user.isAdmin)
            {
                var plan = await db.CreditPlans.FirstOrDefaultAsync(plan => plan.Id == id);
                plan.Status = CreditPlanStatus.Disabled;
                await db.SaveChangesAsync();
            }

            return RedirectToAction("Index", "Home");
        }
    }
}