﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using InstantCredit.Models;
using InstantCredit.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using System.Data.Entity;
using System.IO;

namespace InstantCredit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly InstantCreditContext db;
        private readonly FileStorage fileStorage;

        public FilesController(InstantCreditContext context, IOptions<FileStorage> fileStorage)
        {
            this.db = context;
            this.fileStorage = fileStorage.Value;
        }

        [Authorize]
        [HttpGet("{id}/passportFile")]
        public async Task<IActionResult> GetPassport(int id)
        {
            var application = await db.CreditApplications.FirstOrDefaultAsync(a => a.Id == id);
            var user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            if (!user.isAdmin || application.User.Email != User.Identity.Name)
            {
                return RedirectToAction("Index", "Home");
            }


            var path = Path.Combine(Directory.GetCurrentDirectory(), fileStorage.PassportFilesDirectory, application.PassportFile);
            var contentType = MimeTypeMap.GetMimeType(Path.GetExtension(application.PassportFile.Trim('.')));
            byte[] massive = System.IO.File.ReadAllBytes(path);
            return File(massive, contentType, application.PassportFile);
        }

        [Authorize]
        [HttpGet("{id}/incomeStatementFile")]
        public async Task<IActionResult> GetIncomeStatement(int id)
        {
            var application = await db.CreditApplications.FirstOrDefaultAsync(a => a.Id == id);
            var user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            if (!user.isAdmin || application.User.Email != User.Identity.Name)
            {
                return RedirectToAction("Index", "Home");
            }


            var path = Path.Combine(Directory.GetCurrentDirectory(), fileStorage.PassportFilesDirectory, application.IncomeStatementFile);
            var contentType = MimeTypeMap.GetMimeType(Path.GetExtension(application.IncomeStatementFile.Trim('.')));
            byte[] massive = System.IO.File.ReadAllBytes(path);
            return File(massive, contentType, application.IncomeStatementFile);
        }
    }
}