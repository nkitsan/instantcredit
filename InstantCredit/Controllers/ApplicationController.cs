﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using InstantCredit.Models;
using InstantCredit.Models.Enums;
using InstantCredit.ViewModels;
using InstantCredit.Core;

namespace InstantCredit.Controllers
{
    public class ApplicationController : Controller
    {
        private readonly InstantCreditContext db;
        private readonly FileService fileService;

        public ApplicationController(InstantCreditContext context)
        {
            db = context;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            List<CreditApplication> creditApplications;

            if (user.isAdmin)
            {
                creditApplications = db.CreditApplications
                    .Where(application => application.Status == ApplicationStatus.NotViewed || application.Status == ApplicationStatus.Approved)
                    .ToList();
            }
            else
            {
                creditApplications = db.CreditApplications
                        .Where(application => 
                            application.User.Email == User.Identity.Name &&
                            application.Status != ApplicationStatus.Done)
                        .ToList();
            }

            return View(creditApplications);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (!user.isAdmin)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create(CreateApplicationModel model)
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);+
            if (!user.isAdmin)
            {
                return RedirectToAction("Index", "Home");
            }

            if (ModelState.IsValid)
            {
                var creditPlan = await db.CreditPlans.FirstOrDefaultAsync(c => c.Id == model.CreditPlan);

                db.CreditApplications.Add(new CreditApplication
                {
                    PassportFile = await fileService.SavePassportFileAsync(model.PassportFile),
                    IncomeStatementFile = await fileService.SaveIncomeStatmentAsync(model.IncomeStatementFile),
                    ContactPhone = model.ContactPhone,
                    CreditPlan = creditPlan,
                    User = user
                }); ;
                await db.SaveChangesAsync();

                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }
    }
}