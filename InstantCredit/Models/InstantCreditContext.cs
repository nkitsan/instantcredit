﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace InstantCredit.Models
{
    public class InstantCreditContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<CreditPlan> CreditPlans { get; set; }
        public DbSet<CreditApplication>CreditApplications { get; set; }
        public DbSet<Payment> Payments { get; set; }

        public InstantCreditContext(DbContextOptions<InstantCreditContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
