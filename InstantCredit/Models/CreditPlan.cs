﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using InstantCredit.Models.Enums;

namespace InstantCredit.Models
{
    public class CreditPlan
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Period { get; set; }
        public float Percents { get; set; }
        public string Description { get; set; }
        public CreditPlanStatus Status { get; set; }
    }
}
