﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using InstantCredit.Models.Enums;

namespace InstantCredit.Models
{
    public class CreditApplication
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string PassportFile { get; set; }
        public string IncomeStatementFile { get; set; }
        public string ContactPhone { get; set; }
        public ApplicationStatus Status { get; set; }
        public string Comment { get; set; }
        public User User { get; set; }
        public CreditPlan CreditPlan { get; set; }
    }
}
