﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InstantCredit.Models.Enums
{
    public enum PaymentStatus
    {
       NextMonth,
       Done,
       Overdue
    }
}
